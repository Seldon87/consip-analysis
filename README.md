# consip-analysis

This repo presents an analysis of the open-consip data (https://dati.consip.it/). Particularly, it presents an analysis on the direct orders performed by a PA on the MePa platform.

For a complete description of the analyzed problem, code, results of the analysis and conclusions, please refer to this [notebook](https://gitlab.com/Seldon87/consip-analysis/-/blob/master/price_forecast_combined_embeddings.ipynb).